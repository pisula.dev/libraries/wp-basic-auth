<?php

namespace IC\Functionality;

class BasicAuth {

	/**
	 * @var string
	 */
	private string $login;

	/**
	 * @var string
	 */
	private string $password;

	/**
	 * @param string $login    .
	 * @param string $password .
	 */
	public function __construct( string $login = 'dev', string $password = 'dev' ) {
		$this->login    = $login;
		$this->password = $password;
	}

	public function hooks(): void {
		add_action( 'template_redirect', [ $this, 'template_redirect' ], PHP_INT_MAX );
	}

	/**
	 * Basic Auth run.
	 */
	public function template_redirect(): void {
		if ( $this->should_not_display_basic_auth() ) {
			return;
		}

		if ( ! defined( 'DONOTCACHEPAGE' ) ) {
			define( 'DONOTCACHEPAGE', true );
		}

		nocache_headers();

		if ( $this->is_not_authenticated() ) {
			header( 'WWW-Authenticate: Basic realm="Access denied"' );

			wp_die( wp_kses_post( $this->get_error_message() ), 401, );
		}
	}

	/**
	 * @return string
	 */
	private function get_error_message(): string {
		return sprintf( '%s <a href="%s">%s</a>', __( 'Sorry, you are not allowed to access this page.' ), esc_url( wp_login_url( admin_url() ) ), __( 'Log in' ) );
	}

	/**
	 * @return bool
	 */
	private function is_not_authenticated(): bool {
		$has_supplied_credentials = ! ( empty( $_SERVER['PHP_AUTH_USER'] ) && empty( $_SERVER['PHP_AUTH_PW'] ) );

		return ! $has_supplied_credentials || $_SERVER['PHP_AUTH_USER'] !== $this->login || $_SERVER['PHP_AUTH_PW'] !== $this->password;
	}

	/**
	 * @return bool
	 */
	private function should_not_display_basic_auth(): bool {
		return is_user_logged_in() || ic_is_production() || ic_is_local();
	}
}
