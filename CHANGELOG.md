## [1.0.1] - 2022-02-21
### Fixed
- basic auth popup

## [1.0.0] - 2022-01-23
### Added
- initial version
